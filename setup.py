from setuptools import setup

setup(name='circsat', 
        version='0.1', 
        description='Modules for manipulating combinational logic circuits for CIRCUIT-SAT', 
        url='', 
        author='Peter Demetriou', 
        author_email='peter.demetriou@students.wits.ac.za', 
        license='Apache-2.0',
        packages=['circsat'],
        zip_safe=False
)
