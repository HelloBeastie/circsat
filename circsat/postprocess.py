class Clauses:
    '''
    Allows calculation of MCGS from MCSC 
    a CNF Circuit.
    '''

    def __init__(self, cct, clauses):
        self.cct = cct
        self.clauses = clauses

    def minimal_corrected_gates(self):
        mcsgs = list()
        for k,v in self.cct.items():
            for mcsc in self.clauses:
                if mcsc in v:
                    mcsgs.append(k)
        return mcsgs

class Graph(Clauses):
    '''
    Converts a Graph solution into MCSC and MCGS 
    solutions given a graph solution, CNF Circuit 
    and a vertex to clause mapping.
    '''

    def __init__(self, graph_sols, cct_cnf, 
            cct, vertex_enc):
        self.g_sols = graph_sols
        self.cnf = cct_cnf
        self.cct = cct
        self.ve = vertex_enc
        super().__init__(self.cct, 
                self.literals_to_mcscs)

    @property
    def vertex_to_literals(self):
        lit_sols = list()
        for g_sol in self.g_sols:
            lit_sol = list()
            lit_sols.append(lit_sol)
            for lit in g_sol:
                lit_sol.append(self.ve[lit])
        return lit_sols
    
    @property
    def literals_to_mcscs(self):
        mcscs = list()
        for lit_sols in self.vertex_to_literals:
            mcsc = self.cnf.copy()
            for lit_sol in lit_sols:
                for clause in mcsc:
                    if lit_sol in clause:
                        mcsc.remove(clause)
            mcscs += mcsc
        return mcscs
