from functools import reduce
from itertools import count

class Circuit:
    '''
    Transforms a netlist of gates along with their IO labels into
    an object that can be used by the pysat module to create a WCNF 
    file describing the PMAX-SAT problem. As such it accepts IO 
    labels for the circuit along with a dictionary of gates that 
    are statically created by the Circuit class itself.
    '''

    def __init__(self, inputs, outputs, **kwargs):
        self.inputs = inputs
        self.outputs = outputs
        self.cct_cnf = reduce(lambda g1,g2: g1+g2, kwargs.values())

    @staticmethod
    def NOT(i, o):
        return [[-i, -o], [i, o]]

    @staticmethod
    def BUFFER(i, o):
        return [[-i, o], [i, -o]]

    @staticmethod
    def AND(i0, i1, o):
        return [[-i0, -i1, o], [i0, -o], [i1, -o]]

    @staticmethod
    def NAND(i0, i1, o):
        return [[-i0, -i1, -o], [i0, o], [i1, o]]

    @staticmethod
    def OR(i0, i1, o):
        return [[i0, i1, -o], [-i0, o], [-i1, o]]

    @staticmethod
    def NOR(i0, i1, o):
        return [[i0, i1, o], [-i0, -o], [-i1, -o]]

    @staticmethod
    def XOR(i0, i1, o):
        return [[-i0, -i1, -o], [i0, i1, -o],
                [i0, -i1, o], [-i0, i1, o]]
    
    @staticmethod
    def XNOR(i0, i1, o):
        return [[-i0, -i1, o], [i0, i1, o],
                [i0, -i1, -o], [-i0, i1, -o]]
    
    @property
    def constraints(self):
        return [[l] for l in self.inputs + self.outputs] 

    @property
    def weights(self):
        return [1 for _ in range(len(self.cct_cnf))]

    @property
    def top_weight(self):
        return 1 + len(self.cct_cnf)

from itertools import combinations, chain
import networkx as nx

class Tseitin():
    '''
    Transforms WCNF into a graph with vertices and edges 
    such that some optimisation problem can be solved on 
    the resultant structure.
    '''
    def __init__(self, cct_cnf):
        self.constraints = cct_cnf.hard
        self.cct_cnf = cct_cnf.soft
        self.cs = int(cct_cnf.comments[0][-1])
        self.inputs = self.flatten_iter(self.constraints[:self.cs])
        self.outputs = self.flatten_iter(self.constraints[self.cs:])
        self.edges = self.edge_combinations + self.consistent_assignments
        self.graph = nx.Graph(self.edges)

    def __label_vertices(self):
        label = 0
        lv = list()
        for clause in self.cct_cnf:
            clauses = list()
            lv.append(clauses)
            for literal in clause:
                clauses.append(label)
                label += 1
        return lv

    def flatten_iter(self, itr):
        return list(chain(*itr))

    def remove_duplicates(self, itr):
        return tuple(set(self.flatten_iter(itr)))

    @property
    def vertex_enc(self):
        return {v:c for v,c in enumerate(
            self.flatten_iter(self.cct_cnf))}

    def constraint_enc(self, cnf_io):
        graph_io = list()
        for l in cnf_io:
            for k,v in self.vertex_enc.items():
                if l == v:
                    graph_io.append(k)
        return graph_io

    def neighbours(self, nodes):
        return [list(self.graph.neighbors(n)) for n in nodes]

    @property
    def graph_on_off(self):
        cct_constraints = self.inputs + self.outputs
        on = self.constraint_enc(cct_constraints)
        off_dup = self.neighbours(on)
        off = self.remove_duplicates(off_dup)
        overlap = set(on).intersection(set(off))
        on = list(set(on) - overlap)
        off = list(set(off) - overlap)
        overlap = list(overlap)
        return on, off, overlap

    @property
    def edge_combinations(self):
        '''
        Edges from adjacent vertices forces at least one 
        literal in each clause to be true
        '''
        return self.flatten_iter([list(combinations(clause, 2))
                for clause in self.__label_vertices()])

    @property
    def consistent_assignments(self):
        '''
        Connecting edges between all literals and their 
        negations forces consistent truth assignments
        '''
        negations = list()
        for ki, ko in combinations(self.vertex_enc, 2):
            if self.vertex_enc[ki] == -1*self.vertex_enc[ko]:
                negations.append((ki, ko))
        return negations
