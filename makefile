.PHONY = help setup test run-mini-sat clean-run

.DEFAULT_GOAL = setup

help:
	@echo "To install circsat type make setup"
	@echo "To run the mini-sat solver type make run-mini-sat"
	@echo "To clean the build artefacts type make clean-build"
	@echo "To clean the run artefacts type make clean-run"

setup:
	sudo python setup.py install

run-mini-sat:
	python masters/mini-sat.py

clean-build:
	sudo rm -r build/ dist/ *.egg-info/
	
clean-run:
	rm *wcnf
