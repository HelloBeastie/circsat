import networkx as nx
import matplotlib.pyplot as plt
import argparse
import fnmatch
import json
import re
import os
from pysat.examples.mcsls import MCSls
from pysat.formula import WCNF
from circsat.postprocess import Clauses

def write_result(filename, result):
    with open(f'{filename}.json', 'w') as _:
        _.write(json.dumps(result, indent=4))

def find_match(directory, string):
    files = list()
    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, string):
            files.append(file)
    return files

parser = argparse.ArgumentParser('mini-sat.py')
parser.add_argument(
        'circsat_folder', 
        help='Defines the folder name of the CIRCSAT problem to be run. ' \
            + 'Usually takes values: onetwodecoder, halfadder, halfsubtractor, ' \
            + 'twoonemux, fulladder, twofourdecoder, fullsubtractor', 
        type=str
)
args = parser.parse_args()

wd = f'circuits/{args.circsat_folder}/'
sd = f'{wd}minisat22/'
if not os.path.exists(sd): os.makedirs(sd)

problem_files = sorted(find_match(wd, 'E*.wcnf'))
circuit_files = sorted(find_match(wd, 'C*.json'))

for cf, pf in zip(circuit_files, problem_files):
    circuit = json.load(open(f'{wd}{cf}', 'r'))
    wcnf = WCNF(from_file=f'{wd}{pf}')
    problem = re.sub(r'\.wcnf', r'', pf)
    mcsls = MCSls(wcnf, use_cld=True, solver_name='m22', use_timer=True)

    mcscs = list()
    for mcs in mcsls.enumerate():
        mcsls.block(mcs)
        for p in mcs:
            mcsc = wcnf.soft[p - 1]
            print(f'Minimal corrected subset clause is: {mcsc}')
            mcscs.append(mcsc)

    print(f'Minimal corrected subset clauses are: {mcscs}')
    time = mcsls.oracle_time()
    print(f'Oracle time: {time}')

    clauses_to_gates = Clauses(circuit, mcscs)
    mcsgs = clauses_to_gates.minimal_corrected_gates()
    print(f'Minimal corrected gates: {mcsgs} for {problem} in {cf}\n')

    result = {
            'oracle_time': time,
            'min_corr_sub_cl': mcscs,
            'min_corr_sub_gates': mcsgs,
            'circuit': circuit,
            'n_gates': len(circuit),
            'problem_name': f'{problem}'
    }
    write_result(f'{sd}/RESULT_{problem}', result)
